asoundrc configuration file for SteelSeries Arctis 7.  It uses the device name to find the device so it should work with any system.  

If you don't want to use pulse audio, this may work for you.  No promises.  Fits my use case.

- Enables two channels(stereo) on game_mix
- Allows selection of chat_mix which uses the dmix plugin as well to allow access to multiple programs
- Allow use of the chat mix dial on the headset as long as you configure programs properly, no different
than how you would do it on Windows.
- Enables microphone obviously...

If you have problems, use apulse for launching programs.  If programs depend on pulseaudio and this doesn't
work for you, sorry.  I'm no alsa or asoundrc guru.
